//
//  ViewController.swift
//  mundoanimal_22001948
//
//  Created by COTEMIG on 17/11/22.
//

import UIKit
import Alamofire
import Kingfisher
import SwiftUI

struct Animal: Decodable {
    let name: String
    let latin_name: String
    let image_link: String
}

class ViewController: UIViewController{
  
   
    @IBOutlet weak var nome: UILabel!
    @IBOutlet weak var imagem: UIImageView!
    @IBOutlet weak var latim: UILabel!
    
    
    override func viewDidLoad() {
    super.viewDidLoad()
    getAnimal()
}
   
    func getAnimal(){
    AF.request("https://zoo-animal-api.herokuapp.com/animals/rand")
        .responseDecodable(of: [Animal].self){
        response in
            if let lista = response.value{
                self.imagem.kf.setImage(with: URL(string:Animal.image_link))
                self.nome.text = Animal.name
                self.latim.text = Animal.latin_name
            }
        }
    }
}



